Unofficial Anime-Planet Anime/Manga List Downloader
===================================================

This Ruby script uses Selenium Webdriver to scrape the contents of a user anime
or manga list on [Anime-Planet](http://www.anime-planet.com) and dumps whatever
it finds as a JSON file.

It thus requires that the Webdriver gem (`gem install selenium-webdriver`), and
Firefox, are installed on the machine that it will be run on.

**Please note that this is neither supported nor endorsed by Anime-Planet; it's
completely unofficial.**

Background
----------

I have had an account on Anime-Planet for a few years, and wanted to import the
data over to my new [AniList](http://anilist.co) profile automatically (AniList
can import [MyAnimeList](http://myanimelist.net) .XML data, but it doesn't have
any _direct_ support for other sites).

Unfortunately, Anime-Planet doesn't currently offer any sort of public API, nor
does it have any way of exporting user data as XML/JSON/etc. which prevented me
from simply downloading my profile data and uploading to AniList. Additionally,
it doesn't seem to be possible to "directly" download the page HTML (presumably
because CloudFlare DDOS protection gets in the way), but Selenium Webdriver can
be used to automatically steer a web browser through the anime/manga lists just
as a human user would, scraping the data as it goes.

How To Use
----------

Assuming that Ruby and Selenium Webdriver are installed, you should simply need
to do the following:

+ Open a terminal window, enter `ruby ap-download.rb`, and press return.
+ You will be prompted for an Anime-Planet username; enter it and press return.
+ After a short delay, Firefox will automatically open and start scraping data.
+ **Wait until it's finished**; it will print a "Done!" message once it's over.
+ You should now have two new files: `anime.json` and `manga.json`.

Note: If you do not enter a username, the script will stop without doing anything.

TODO
----

It would probably be more useful to write the scraped data as MyAnimeList-esque
XML, allowing it to be imported directly into AniList...

License
-------

Copyright (c) 2016, Thomas Glyn Dennis

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
