#!/usr/bin/ruby

# COMPLETELY, TOTALLY 100% UNOFFICIAL ANIME-PLANET LIST DOWNLOAD SCRIPT
#
# Copyright (c) 2016, Thomas Glyn Dennis
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

require 'selenium/webdriver'  # Ubuntu: `sudo gem install selenium-webdriver`
require 'json'

# Scrape the anime/manga list of an Anime-Planet user and return it as a hash.
#
# Anime-Planet doesn't currently offer any sort of public API, nor does it have
# any way of exporting user data as XML/JSON/etc. so this is all we can do...
#
# It doesn't seem to be possible to "directly" download the page HTML any more,
# presumably because CloudFlare gets in the way, so we use "Selenium Webdriver"
# to steer a web browser through the user profile pages, grabbing their details
# as we go.
#
# @param user The username you're interested in.
# @param list Should be either "anime" or "manga".
# @return A hash representing the data scraped from the user profile.

def anime_planet_data(user, list)
  results = []
  # If you want to use `:chrome`, make sure that `chromedriver` is installed.
  browser = Selenium::WebDriver.for :firefox
  browser.manage.timeouts.implicit_wait = 4 # seconds

  # Keep searching until we've seen all pages:
  (1..9999).each do |page|
    puts "\tDownloading page #{page}..."
    browser.get "http://www.anime-planet.com/users/#{user}/#{list}?page=#{page}"

    # Scrape the page for data. The format varies; here are a few examples:
    # <span class="status5"></span> 4 eps<div class="ttRating">3.5</div>
    # <span class="status3"></span> 3 eps
    # <span class="status4"></span> Want to Watch
    # <span class="status5"></span> Stalled<div class="ttRating">3</div>

    results += browser.execute_script %Q|
      var result = [];
      var e = document.querySelectorAll(".card");
      for (var i = 0; i < e.length; ++i) {
        var data = { name: e[i].querySelector("h4").innerHTML };
        var html = e[i].querySelector(".statusArea").innerHTML;

        // Look at the CSS class used for the icon to determine the status:
        data.status = {
          "1": "Watched",
          "2": "Watching",
          "3": "Dropped",
          "4": "Want To Watch",
          "5": "Stalled",
          "6": "Won't Watch"
        }[html.slice(19, 20)];

        // The rating text may or may not be present, since they're optional:
        var tmp = html.indexOf("ttRating");
        if (tmp >= 0) { data.rating = html.slice(tmp + 10, html.length - 6); }

        // Something like "3 eps" is fine; anything else should be ignored:
        var tmp = html.split("</span>")[1].split("<div>")[0];
        if (tmp.match('\\\\d')) { data.episodes = tmp; }

        result.push(data);
      }
      return result;
    |

    # Stop here if there is no "next" page link (i.e. we're at the end):
    has_more = "return document.querySelector('a[href*=\"page=#{page + 1}\"]')"
    break unless browser.execute_script(has_more)
  end

  # Close the browser and return the results:
  browser.close
  results
end

# Grab the Anime and Manga data from Anime-Planet and save it in JSON format.
print "Please enter your Anime-Planet username: "
username = gets.gsub(/\W/, '')
if username.empty?
  puts "No username given; nothing done."
else
  %w(anime manga).each do |list|
    puts "Downloading #{list} list..."
    File.open("#{list}.json", 'w') do |file|
      file.write JSON::generate(anime_planet_data(username, list))
    end
  end
  puts "Done!"
end
